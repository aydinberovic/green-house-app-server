const express = require("express");
const app = express();
const port = 3001;

app.use(express.json());

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Access-Control-Allow-Headers"
  );
  next();
});

let greenHouseId = 900;
let tag = 1;

const greenHouses = [];

app.get("/", (req, res) => {
  let green = {};
  green.id = greenHouseId;
  green.name = "Sera " + tag++;
  green.temp = 0;
  green.isPaused = false;
  console.log("id: ", greenHouseId);
  console.log("green: ", green);

  greenHouses.push(green);
  res.status(200).send(JSON.stringify(greenHouseId));
  greenHouseId++;
  console.log("id: ", greenHouseId);
});

app.get("/greenHouses", (req, res) => {
  res.status(200).send(JSON.stringify(greenHouses));

  console.log("greenHouses: ", greenHouses);
});

app.post("/greenHouses", (req, res) => {
  console.log(req.body);

  index = greenHouses.findIndex((obj) => obj.id == req.body.id);
  greenHouses[index].isPaused = req.body.isPaused;
  res.status(200).send(JSON.stringify(greenHouses));
  console.log("greenHouses: ", greenHouses);
});

app.delete("/greenHouses/:id", (req, res) => {
  console.log("params: " + req.params.id);
  index = greenHouses.findIndex((obj) => obj.id == req.params.id);

  greenHouses.splice(index, 1);
  res.status(200).send(JSON.stringify(greenHouses));
  console.log("greenHouses: ", greenHouses);
});

app.post("/changeGreenHouse", (req, res) => {
  console.log(req.body);
});

app.post("/createGreenHouse", (req, res) => {
  console.log(req.body);
});

app.listen(port, () => {
  console.log(`Server running on port ${port}.`);
});
